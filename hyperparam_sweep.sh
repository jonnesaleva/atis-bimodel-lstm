#!/usr/bin/env bash

NUM_LAYERS=(1 2)
P_DROPOUT=(0.2 0.4 0.6)
NUM_EPOCHS=(25 50 100 300)
GPU=$1

for num_epochs in "${NUM_EPOCHS[@]}"; do
    for num_enc_layers in "${NUM_LAYERS[@]}"; do
        for num_dec_layers in "${NUM_LAYERS[@]}"; do
            for p_dropout in "${P_DROPOUT[@]}"; do
                LOGDIR="./experiments/exp${GPU}"
                mkdir -p $LOGDIR
                LOGFILE="${LOGDIR}/${num_enc_layers}enc"
                LOGFILE="${LOGFILE}.${num_dec_layers}dec"
                LOGFILE="${LOGFILE}.${p_dropout}dropout"
                LOGFILE="${LOGFILE}.${num_epochs}epochs.log"
                echo "Logging to $LOGFILE" | tee $LOGFILE
                CUDA_VISIBLE_DEVICES=$GPU python -u train.py \
                    --train-data-path ./data/data-from-miulab/atis/train \
                    --dev-data-path ./data/data-from-miulab/atis/valid \
                    --test-data-path ./data/data-from-miulab/atis/test \
                    --num-epochs "${num_epochs}" \
                    --enc-num-layers "${num_enc_layers}" \
                    --dec-num-layers "${num_dec_layers}" \
                    --p-dropout "${p_dropout}" \
                    --device cuda | tee -a "${LOGFILE}"
            done
        done
    done
done
