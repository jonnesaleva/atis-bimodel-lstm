# https://pytorch.org/tutorials/beginner/torchtext_translation_tutorial.html

from torch.nn.utils.rnn import pad_sequence
from torch.utils.data import DataLoader
from torchtext.vocab import Vocab
import torchtext
import torch

from collections import Counter, defaultdict
import itertools as it
import io
import os

from typing import List, Any, Dict, Tuple
import pandas as pd
import click


def load_data_miulab(path: str, resolve_intent: str = "left") -> pd.DataFrame:
    """
    resolve_intent = "left":
        resolve("a#b") = "a"
    resolve_intent = "right":
        resolve("a#b") = "b"
    resolve_intent = "both":
        resolve("a#b") = "a#b"
    """
    name_to_filename = {
        "intent": "label",
        "sentence": "seq.in",
        "slots": "seq.out",
    }
    lines = defaultdict(list)
    for name, fname in name_to_filename.items():
        full_path = os.path.abspath(path)
        with open(f"{full_path}/{fname}", encoding="utf-8") as f:
            lines[name].extend(l.strip() for l in f.readlines())
    columns = ["intent", "sentence", "slots"]
    df = pd.DataFrame(lines, columns=columns)
    if resolve_intent != "both":

        def parse_intent(i):
            if "#" not in i:
                return i
            else:
                return i.split("#")[int(resolve_intent == "right")]

        df.intent = df.intent.apply(parse_intent)

    return df


class ATISDataset(torch.utils.data.Dataset):
    def __init__(
        self,
        csv_path: str,
        sentence_column: str = "sentence",
        slot_column: str = "slots",
        intent_column: str = "intent",
        pad_token: str = "<pad>",
        unk_token: str = "<unk>",
        resolve_intent: str = "left",
    ) -> None:
        super(ATISDataset, self).__init__()
        self.csv_path = os.path.abspath(csv_path)
        self.pad_token = pad_token
        self.unk_token = unk_token
        self.bos_token = "BOS"  # these are hardcoded due to the dataset
        self.eos_token = "EOS"  # these are hardcoded due to the dataset
        self.resolve_intent = resolve_intent
        self.sentence_column = sentence_column
        self.slot_column = slot_column
        self.intent_column = intent_column

        # we only want to load this once
        # self.csv_file = pd.read_csv(self.csv_path, encoding="utf-8")
        self.csv_file = load_data_miulab(self.csv_path, self.resolve_intent)

        # once we have the CSV we can create vocab objects
        self.sentence_vocab = self.build_vocab(self.sentence_column)
        self.slots_vocab = self.build_vocab(self.slot_column)
        self.intent_vocab = self.build_vocab(self.intent_column)
        self.vocabs = {
            "tokens": self.sentence_vocab,
            "slots": self.slots_vocab,
            "intent": self.intent_vocab,
        }

        # and also the (sentence, slots, intent) tuples
        self.data = self.process_data()

    def tokenizer(self, s: str) -> List[str]:
        return s.split()

    def flatten(self, nested: List[List[Any]]) -> List[Any]:
        return list(it.chain.from_iterable(nested))

    def build_vocab(
        self,
        column_name: str,
    ) -> Vocab:
        flat_elems = self.flatten(
            self.csv_file[column_name].apply(self.tokenizer)
        )
        return Vocab(
            Counter(flat_elems),
            specials=[
                self.unk_token,
                self.pad_token,
                self.bos_token,
                self.eos_token,
            ],
        )

    def process_data(
        self,
    ) -> List[Tuple[torch.Tensor, torch.Tensor, torch.Tensor]]:
        """Loads CSV, iterates over the rows, and numericalizes the data."""
        data = []
        for ix, row in self.csv_file.iterrows():
            tokens_tensor = torch.tensor(
                [
                    self.sentence_vocab[t]
                    for t in self.tokenizer(row[self.sentence_column])
                ],
                dtype=torch.long,
            )
            slots_tensor = torch.tensor(
                [
                    self.slots_vocab[s]
                    for s in self.tokenizer(row[self.slot_column])
                ],
                dtype=torch.long,
            )
            intent_tensor = torch.tensor(
                self.intent_vocab[row[self.intent_column]],
                dtype=torch.long,
            )
            data.append((tokens_tensor, slots_tensor, intent_tensor))
        return data

    def __len__(self):
        return len(self.data)

    def __getitem__(self, ix):
        return self.data[ix]

    @property
    def labels(self) -> set:
        return set(self.csv_file.intent)


def generate_batch(
    data_batch: Any,
    pad_ix: int,
    bos_ix: int,
    eos_ix: int,
    batch_first: bool = False,
) -> Tuple[List[torch.tensor], List[torch.tensor], List[torch.tensor]]:
    """Generates batches, used as collate_fn"""
    tokens_batch, slots_batch, intent_batch = [], [], []
    for (_tokens, _slots, _intent) in data_batch:
        tokens_batch.append(_tokens)
        slots_batch.append(_slots)
        intent_batch.append(_intent)
    tokens_batch = pad_sequence(tokens_batch, padding_value=pad_ix)
    slots_batch = pad_sequence(slots_batch, padding_value=pad_ix)
    intent_batch = torch.tensor(intent_batch)

    if batch_first:
        tokens_batch = tokens_batch.permute(-1, 0)
        slots_batch = slots_batch.permute(-1, 0)

    return tokens_batch, slots_batch, intent_batch


def create_data_and_vocab(
    train_path: str,
    valid_path: str,
    test_path: str,
    pad_token: str = "<pad>",
    unk_token: str = "<unk>",
    batch_size: int = 64,
    batch_first: bool = False,
):

    train_data = ATISDataset(
        train_path, pad_token=pad_token, unk_token=unk_token
    )
    valid_data = ATISDataset(
        valid_path, pad_token=pad_token, unk_token=unk_token
    )
    test_data = ATISDataset(
        test_path, pad_token=pad_token, unk_token=unk_token
    )

    sentence_vocab = train_data.sentence_vocab

    def _generate_batch(data_batch):
        bos_ix = sentence_vocab[train_data.bos_token]
        eos_ix = sentence_vocab[train_data.eos_token]
        pad_ix = sentence_vocab[train_data.pad_token]
        return generate_batch(
            data_batch,
            pad_ix=pad_ix,
            bos_ix=bos_ix,
            eos_ix=eos_ix,
            batch_first=batch_first,
        )

    train_iter = DataLoader(
        train_data,
        batch_size=batch_size,
        shuffle=True,
        collate_fn=_generate_batch,
    )
    valid_iter = DataLoader(
        valid_data,
        batch_size=batch_size,
        shuffle=True,
        collate_fn=_generate_batch,
    )
    test_iter = DataLoader(
        test_data,
        batch_size=batch_size,
        shuffle=True,
        collate_fn=_generate_batch,
    )

    return train_iter, valid_iter, test_iter, train_data.vocabs


if __name__ == "__main__":
    create_data_and_vocab()
