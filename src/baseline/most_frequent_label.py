from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction import DictVectorizer
from sklearn.metrics import accuracy_score
import pandas as pd
import numpy as np
import click

from collections import Counter
import os


def extract_features(tokens):
    """Extract dumb BoW features"""
    features = {"bias": 1.0}
    for tok in tokens:
        features[f"tok={tok}"] = 1.0
    return features


def get_data(path):
    path = os.path.abspath(path)
    data = pd.read_csv(path, encoding="utf-8")
    sentences = data.tokens.str.replace("BOS", "").str.replace("EOS", "")
    intents = data.intent
    return sentences, intents


class MostFrequentLabelClassifier:
    def fit(self, X, y):
        self.label = Counter(y).most_common()[0][0]

    def predict(self, X):
        return np.array([self.label for _ in X])


@click.command()
@click.option("--train-data-path", required=True)
@click.option("--test-data-path", required=True)
def main(train_data_path, test_data_path):

    # load train/test data
    sentences_train, intents_train = get_data(train_data_path)
    sentences_test, intents_test = get_data(test_data_path)

    # generate features
    dv = DictVectorizer()
    train_features = dv.fit_transform(sentences_train.apply(extract_features))
    test_features = dv.transform(sentences_test.apply(extract_features))

    # log reg
    clf = MostFrequentLabelClassifier()
    clf.fit(train_features, intents_train)
    intents_train_pred = clf.predict(train_features)
    intents_test_pred = clf.predict(test_features)

    # classification report
    acc_train = accuracy_score(intents_train, intents_train_pred)
    acc_test = accuracy_score(intents_test, intents_test_pred)
    print(f"Intent accuracy (train): {acc_train:.4f}")
    print(f"Intent accuracy (test): {acc_test:.4f}")


if __name__ == "__main__":
    main()
