# model.py
# Defines the bi-model net(s) from the paper
# "A Bi-model based RNN Semantic Frame Parsing Model for Intent Detection and Slot Filling".
# by Yu Wang, Yilin Shen, Hongxia Jin

# minor inspiration taken from
# https://github.com/ray075hl/Bi-Model-Intent-And-Slot

import torch
import torch.nn as nn
import torch.nn.functional as F

class WangShenJinNetWithDecoder(nn.Module):
    def __init__(
        self,
        vocab_size,
        embedding_dim,
        enc_hidden_size,
        dec_hidden_size,
        p_dropout,
        n_intents,
        n_slots,
        enc_num_layers=2,
        dec_num_layers=2,
        with_dropout=False
    ):
        super().__init__()

        self.vocab_size = vocab_size
        self.embedding_dim = embedding_dim
        self.enc_hidden_size = enc_hidden_size
        self.dec_hidden_size = dec_hidden_size
        self.p_dropout = p_dropout
        self.n_intents = n_intents
        self.n_slots = n_slots
        self.enc_num_layers = enc_num_layers
        self.dec_num_layers = dec_num_layers
        self.with_dropout = with_dropout

        # shared embeddings
        self.embedding = nn.Embedding(
            # +2 comes from [unk, pad]
            num_embeddings=self.vocab_size,  # + 2,
            embedding_dim=self.embedding_dim,
            padding_idx=0,
        )

        # intent bi-lstm encoder
        self.f1 = nn.LSTM(
            self.embedding_dim,
            self.enc_hidden_size,
            self.enc_num_layers,
            batch_first=True,
            bidirectional=True,
        )

        # intent lstm decoder
        self.g1 = nn.LSTM(
            4 * self.enc_hidden_size,
            self.dec_hidden_size,
            self.dec_num_layers,
            batch_first=True,
            bidirectional=False,
        )

        # intent logit transform before softmax
        self.fc1 = nn.Linear(self.dec_hidden_size, self.n_intents)

        # slot bi-lstm encoder
        self.f2 = nn.LSTM(
            self.embedding_dim,
            self.enc_hidden_size,
            self.enc_num_layers,
            batch_first=True,
            bidirectional=True,
        )

        # slot lstm decoder
        self.g2 = nn.LSTM(
            4 * self.enc_hidden_size,
            self.dec_hidden_size,
            self.dec_num_layers,
            batch_first=True,
            bidirectional=False,
        )

        # slot logit transform before softmax
        self.fc2 = nn.Linear(self.dec_hidden_size, self.n_slots)

        # finally a dropout layer
        self.dropout = nn.Dropout(self.p_dropout)

    def forward(self, x):
        x = self.embedding(x)

        h_intent, _ = self.f1(x)
        if self.with_dropout:
            h_intent = self.dropout(h_intent)
        
        h_slot, _ = self.f2(x)
        if self.with_dropout:
            h_slot = self.dropout(h_slot)

        h_concat = torch.cat((h_intent, h_slot), dim=-1)

        intent_decoder_out, _ = self.g1(h_concat)
        if self.with_dropout:
            intent_decoder_out = self.dropout(intent_decoder_out)
        
        slot_decoder_out, _ = self.g2(h_concat)
        if self.with_dropout:
            slot_decoder_out = self.dropout(slot_decoder_out)

        # (batch_size x seq_len x n_intents)
        intent_logits = self.fc1(intent_decoder_out)
        
        # we only want the last time step's logits
        intent_logits = intent_logits[:, -1, :]
        
        # (batch_size x seq_len x n_slots)
        slot_logits = self.fc2(slot_decoder_out)

        assert intent_logits.ndim == 2
        assert slot_logits.ndim == 3

        return intent_logits, slot_logits
