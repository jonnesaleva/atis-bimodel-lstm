import click

import src.atis.atis_dataset as ads
from src.model import WangShenJinNetWithDecoder
from sklearn.metrics import classification_report

import torch
import torch.nn as nn
import torch.nn.functional as F

import itertools as it
import numpy as np

import sys

BATCH_FIRST = True
PAD_IDX = 1


# other helpers
def flatten(nested, as_numpy=True, reshape=True):
    out = list(it.chain.from_iterable(nested))

    if as_numpy:
        out = np.array(out)

        if reshape:
            out = out.reshape(-1, 1)

    return out


def evaluate_on_dev(net, dev_loader, loss_intent, loss_slots):
    cumul_intent_loss = 0.0
    cumul_slot_loss = 0.0
    with torch.no_grad():
        for (sentences_dev, slots_dev, intents_dev) in dev_loader:
            intent_logits_dev, slot_logits_dev = net(sentences_dev)
            # dev intent loss
            loss_intent_dev = loss_intent(intent_logits_dev, intents_dev)
            # dev slot loss
            B_dev, T_dev, S_dev = slot_logits_dev.shape
            slot_logits_dev_flat = slot_logits_dev.view(B_dev * T_dev, S_dev)
            slots_dev_flat = slots_dev.reshape(B_dev * T_dev)
            loss_slots_dev = loss_slots(slot_logits_dev_flat, slots_dev_flat)

            cumul_intent_loss += loss_intent_dev.item()
            cumul_slot_loss += loss_slots_dev.item()

    return cumul_intent_loss, cumul_slot_loss


def predict(sent, net, vocabs):
    net.eval()
    tokens = sent.split()
    sent_vocab = vocabs["tokens"]
    slot_vocab = vocabs["slots"]
    intent_vocab = vocabs["intent"]
    tokens_numericalized = torch.tensor(
        [sent_vocab.stoi[w] for w in tokens]
    ).unsqueeze(0)
    intent_logits, slot_logits = net(tokens_numericalized)
    intent_pred = F.softmax(intent_logits.squeeze(0), dim=-1).argmax(dim=-1)
    slots_pred = F.softmax(slot_logits.squeeze(0), dim=-1).argmax(dim=-1)
    intent_pred_word = intent_vocab.itos[intent_pred]
    slots_pred_word = [slot_vocab.itos[slot] for slot in slots_pred]
    net.train()

    return intent_pred_word, slots_pred_word


def compute_clf_report(sentences, intents, slots, net, vocabs):
    flat_slots = flatten(slots, as_numpy=False, reshape=False)
    predicted_intents = []
    predicted_slots = []

    for sent_, intent_, slots_ in zip(sentences, intents, slots):
        intent_pred, slots_pred = predict(sent_, net, vocabs,)
        predicted_intents.append(intent_pred)
        predicted_slots.extend(slots_pred)
    intent_report_str = classification_report(intents, predicted_intents)
    intent_report_dict = classification_report(
        intents, predicted_intents, output_dict=True
    )
    slots_report_str = classification_report(flat_slots, predicted_slots)
    slots_report_dict = classification_report(
        flat_slots, predicted_slots, output_dict=True
    )

    return (
        intent_report_str,
        intent_report_dict,
        slots_report_str,
        slots_report_dict,
    )


def get_clf_report(net, vocabs, data_path, should_print=False):
    atis_test = ads.load_data_miulab(data_path)
    (
        intent_report_str,
        intent_report_dict,
        slots_report_str,
        slots_report_dict,
    ) = compute_clf_report(
        sentences=atis_test.sentence.tolist(),
        intents=atis_test.intent.tolist(),
        slots=atis_test.slots.str.split().tolist(),
        net=net,
        vocabs=vocabs,
    )

    if should_print:
        print("Intent report")
        print(intent_report_str)
        print("Slot filling:")
        print(slots_report_str)

    return intent_report_dict, slots_report_dict


# main training function
@click.command()
@click.option("--train-data-path", type=str, required=True)
@click.option("--dev-data-path", type=str, required=True)
@click.option("--test-data-path", type=str, required=True)
@click.option(
    "--num-epochs", type=int, help="Number of epochs to train for", default=1
)
@click.option("--batch-size", type=int, default=64)
@click.option(
    "--max-grad", type=int, help="Clip gradients to this value.", default=5
)
@click.option(
    "--embedding-dim",
    type=int,
    help="Dimension of word vector embeddings",
    default=300,
)
@click.option(
    "--enc-hidden-layer-size",
    type=int,
    help="Dimension of Bi-LSTM encoder",
    default=200,
)
@click.option(
    "--enc-num-layers",
    type=int,
    help="How many layers in the Bi-LSTM encoder?",
    default=1,
)
@click.option(
    "--dec-hidden-layer-size",
    type=int,
    help="Dimension of LSTM decoder",
    default=200,
)
@click.option(
    "--dec-num-layers",
    type=int,
    help="How many layers in the LSTM decoder?",
    default=1,
)
@click.option(
    "--p-dropout", type=float, help="Probability of dropout", default=0.2
)
@click.option("--device", type=str, help="Device to use", default="cpu")
def main(
    train_data_path,
    dev_data_path,
    test_data_path,
    num_epochs,
    batch_size,
    max_grad,
    embedding_dim,
    enc_hidden_layer_size,
    dec_hidden_layer_size,
    enc_num_layers,
    dec_num_layers,
    p_dropout,
    device="cpu",
):

    if device != "cpu":
        print("Chosen device not a CPU! Setting CUDA tensors as default...")
        torch.set_default_tensor_type(torch.cuda.FloatTensor)

    device = torch.device(device)

    # load the data
    (
        train_loader,
        dev_loader,
        test_loader,
        vocabs,
    ) = ads.create_data_and_vocab(
        train_path=train_data_path,
        valid_path=dev_data_path,
        test_path=test_data_path,
        batch_size=batch_size,
        batch_first=True,
    )

    # take note of constants
    VOCAB_SIZE = len(vocabs["tokens"])
    N_SLOTS = len(vocabs["slots"])
    N_INTENTS = len(vocabs["intent"])

    # instantiate the neural network
    wsjn = WangShenJinNetWithDecoder(
        vocab_size=VOCAB_SIZE,
        embedding_dim=embedding_dim,
        enc_hidden_size=enc_hidden_layer_size,
        dec_hidden_size=dec_hidden_layer_size,
        n_intents=N_INTENTS,
        n_slots=N_SLOTS,
        p_dropout=p_dropout,
        enc_num_layers=enc_num_layers,
        dec_num_layers=dec_num_layers,
        with_dropout=bool(p_dropout > 0.0),
    )

    # instantiate cross-entropy losses and Adam optimizer
    xent_intent = nn.CrossEntropyLoss(reduction="sum", ignore_index=PAD_IDX)
    xent_slots = nn.CrossEntropyLoss(reduction="sum", ignore_index=PAD_IDX)
    adam = torch.optim.Adam(wsjn.parameters())

    # actually perform the epoch-based training

    for epoch in range(num_epochs):

        cumul_intent_loss = 0.0
        cumul_slot_loss = 0.0

        for sentences, slots, intents in train_loader:

            sentences = sentences.to(device)
            slots = slots.to(device)
            intents = intents.to(device)

            adam.zero_grad()

            # forward pass
            intent_logits, slot_logits = wsjn(sentences)

            # these need to get flattened for the cross-entropy loss
            B, T, S = slot_logits.shape

            slot_logits_flat = slot_logits.view(B * T, S)
            slots_flat = slots.reshape(B * T)

            # compute the losses for intent & slots
            loss_intent = xent_intent(intent_logits, intents)
            loss_slots = xent_slots(slot_logits_flat, slots_flat)

            # add to cumulative loss
            with torch.no_grad():
                cumul_intent_loss += loss_intent.item()
                cumul_slot_loss += loss_slots.item()

            # propagate gradients
            loss_intent.backward(retain_graph=True)
            loss_slots.backward()

            # clip gradients
            nn.utils.clip_grad_norm_(wsjn.parameters(), max_grad)

            # step the optimizers
            adam.step()

        print(f"Epoch {epoch} done.")
        print(f"Train Intent loss: {cumul_intent_loss}")
        print(f"Train Slot loss: {cumul_slot_loss}")

        with torch.no_grad():
            loss_intent_dev, loss_slots_dev = evaluate_on_dev(
                wsjn, dev_loader, xent_intent, xent_slots
            )

            print(f"Dev Intent loss: {loss_intent_dev}")
            print(f"Dev slot loss: {loss_slots_dev}")

            intent_report_dict_dev, slots_report_dict_dev = get_clf_report(
                wsjn, vocabs, dev_data_path, should_print=False
            )
            intent_acc_dev = round(intent_report_dict_dev["accuracy"], 4)
            slots_f1_dev = round(
                slots_report_dict_dev["weighted avg"]["f1-score"], 4
            )
            print(f"Intent accuracy (dev): {intent_acc_dev}")
            print(f"Slot F1 (dev): {slots_f1_dev}")

    print("Evaluating on test set...")
    intent_report_dict_test, slots_report_dict_test = get_clf_report(
        wsjn, vocabs, test_data_path, should_print=True
    )
    intent_acc_test = round(intent_report_dict_test["accuracy"], 4)
    slots_f1_test = round(
        slots_report_dict_test["weighted avg"]["f1-score"], 4
    )
    print(f"Intent accuracy (test): {intent_acc_test}")
    print(f"Slot F1 (test): {slots_f1_test}")


if __name__ == "__main__":
    main()
